/**
 * Created by vaibhav on 29-11-2016.
 */
import React, {PropTypes} from 'react';
import TextInput from '../common/TextInput'
import MessageInput from '../common/MessageInput';

const ContactForm = ({fullname, email, message, errors, loading, onChange, onSave}) => {
    return (
        <form>
            <div className="row 50%">
                <TextInput name="fullname" value={fullname} placeholder="Name" onChange={onChange} error={errors.title}/>
                <TextInput name="email" value={email} placeholder="Email" onChange={onChange} error={errors.title}/>
            </div>
            <div className="row 50%">
                <MessageInput name="message" value={message} placeholder="Message" onChange={onChange}
                              error={errors.title}/>
            </div>
            <div className="row">
                <div className="12u">
                    <ul className="buttons">
                        <li>
                            <input
                                type="submit"
                                disabled={loading}
                                className="special"
                                onClick={onSave}
                                value={loading ? 'Sending Message...' : 'Send Message'}/>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
    );
};

ContactForm.propTypes = {
    fullname: PropTypes.string,
    email: PropTypes.string,
    message: PropTypes.string,
    loading: PropTypes.bool,
    onSave: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object
};

export default ContactForm;