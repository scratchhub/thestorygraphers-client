/**
 * Created by vaibhav on 27-11-2016.
 */
import React from 'react';

import Header from '../Header';

class BlogPage extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <article id="main">
                    {this.props.children}
                </article>
            </div>
        );
    }
}

export default BlogPage;