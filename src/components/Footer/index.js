/**
 * Created by vaibhav on 27-11-2016.
 */
import React from 'react';

const Footer = () => {
    return(
        <footer id="footer">

            <ul className="icons">
                <li><a href="https://www.youtube.com/channel/UCMvO_l1BYLCz0L_rQNfzl9g/videos"
                       className="icon circle fa-youtube-play"><span className="label">Youtube</span></a></li>
                <li><a href="https://twitter.com/thestorygrapher" className="icon circle fa-twitter"><span
                    className="label">Twitter</span></a></li>
                <li><a href="https://www.facebook.com/thestorygraphers" className="icon circle fa-facebook"><span className="label">Facebook</span></a>
                </li>
                <li><a href="https://instagram.com/thestorygraphers/" className="icon circle fa-instagram"><span className="label">Instagram</span></a>
                </li>
                <li><a href="mailto:mailthestorygraphers@gmail.com" className="icon circle fa-envelope"><span className="label">Email</span></a>
                </li>
            </ul>

            <ul className="copyright">
                <li>&copy; The Storygraphers 2016. All Rights Reserved.</li>
                <li>Powered by : <a href="http://scratchhub.in/" target="_new">Scratch Hub</a></li>
            </ul>

        </footer>
    );
};

export default Footer;