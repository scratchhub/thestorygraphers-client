/**
 * Created by vaibhav on 29-11-2016.
 */
import React, {PropTypes} from 'react';

const MessageInput = ({name, onChange, placeholder, value, error}) => {
    return (
        <div className="12u">
            <textarea name={name} placeholder={placeholder} value={value} onChange={onChange} rows="7"/>
            {error && <p><strong>{error}</strong></p>}
        </div>
    );
};

MessageInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    error: PropTypes.string
};

export default MessageInput;