/**
 * Created by vaibhav on 29-11-2016.
 */
import React, {PropTypes} from 'react';

const TextInput = ({name, onChange, placeholder, value, error}) => {
    return(
        <div className="6u 12u(mobile)">
            <input type="text" name={name} placeholder={placeholder} value={value} onChange={onChange}/>
            {error && <p><strong>{error}</strong></p>}
        </div>
    );
};

TextInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    error: PropTypes.string
};

export default TextInput;