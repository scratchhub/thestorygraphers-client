/**
 * Created by vaibhav on 27-11-2016.
 */
import React from 'react';
import Header from '../Header';
import Banner from '../Banner';
import About from './About';
import RecentProjects from '../../containers/RecentProjects';
import TeamList from '../../containers/TeamList';

const HomePage = () => {
    return (
        <div>
            <Header alt="alt"/>
            <Banner/>
            <article id="main">
                <header className="special container">
                    <span className="icon fa-camera-retro"/>
                    <h2>Who We Are</h2>
                </header>
                <About/>
                <RecentProjects/>
                <TeamList/>
            </article>
        </div>
    );
};

export default HomePage;