/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component} from 'react';
import axios from 'axios';

const TextDisplay = ({about}) => {
    return (
        <p>{about}</p>
    );
};


class About extends Component {
    constructor(props) {
        super(props);
        this.state = {
            aboutDescription: [],
        };

        this.loadAbout = this.loadAbout.bind(this);
    }

    componentWillMount() {
        this.loadAbout();
    }

    loadAbout() {
        axios.get('http://thestorygraphers.pythonanywhere.com/api/about/?format=json').then(function (response) {
            this.setState({aboutDescription: response.data});
        }.bind(this)).catch(function (error) {
            console.log(error);
        });
    }

    render() {
        return (
            <section className="wrapper style2 container special-alt">
                <div className="row 50%">
                    <div className="8u 12u(narrower)">

                        <header>
                            <h2>About Us</h2>
                        </header>
                        <div>{this.state.aboutDescription.map(about =>
                            <TextDisplay key={about.id} about={about.about_desc}/>
                        )}</div>
                    </div>
                    <div className="4u 12u(narrower) important(narrower)">

                        <ul className="featured-icons">
                            <li><span className="icon fa-camera-retro"><span className="label">Photography</span></span>
                            </li>
                            <li><span className="icon fa-photo"><span className="label">Photography</span></span></li>
                            <li><span className="icon fa-video-camera"><span className="label">Film Making</span></span>
                            </li>
                            <li><span className="icon fa-eye"><span className="label">Film Making</span></span></li>
                            <li><span className="icon fa-book"><span className="label">Writers</span></span></li>
                            <li><span className="icon fa-edit"><span className="label">Writers</span></span></li>
                            <li><span className="icon fa-laptop"><span className="label">VFX</span></span></li>
                            <li><span className="icon fa-magic"><span className="label">VFX</span></span></li>
                            <li><span className="icon fa-music"><span className="label">Music</span></span></li>
                            <li><span className="icon fa-lightbulb-o"><span className="label">Music</span></span></li>
                        </ul>

                    </div>
                </div>
            </section>
        );
    }
}


export default About;