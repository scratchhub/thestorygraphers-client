/**
 * Created by vaibhav on 28-11-2016.
 */
import React, {Component} from 'react';
import Header from '../Header';
import PhotoList from '../../containers/PhotoList';

class PhotosPage extends Component {
    render(){
        return(
            <div>
                <Header/>
                <article id="main">
                    <PhotoList/>
                </article>
            </div>
        );
    }
}

export default PhotosPage;