/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component} from 'react';
import {Link} from 'react-router';

class Header extends Component {
    render() {
        const {alt} = this.props;
        return (
            <header id="header" className={alt}>
                <h1 id="logo"><a href="/">The Storygraphers <span>Live A Story</span></a></h1>
                <nav id="nav">
                    <ul>
                        <li className="current"><a href="/">Home</a></li>
                        <li className="submenu">
                            <a href="#">Work</a>
                            <ul>
                                <li><Link to="/photography">Photography</Link></li>
                                <li><Link to="/films">Films</Link></li>
                            </ul>
                        </li>
                        <li><a href="/#team" className="scrolly">Team</a></li>
                        <li><Link to="/blog">Blog</Link></li>
                        <li><Link to="/contact" className="button special">Contact</Link></li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;