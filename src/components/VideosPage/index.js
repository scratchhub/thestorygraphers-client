/**
 * Created by vaibhav on 28-11-2016.
 */
import React, {Component} from 'react';

import VideoList from '../../containers/VideoList';
import Header from '../Header';

class VideosPage extends Component {
    render(){
        return(
            <div>
                <Header/>
                <article id="main">
                    <VideoList/>
                </article>
            </div>
        );
    }
}

export default VideosPage;