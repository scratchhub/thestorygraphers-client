/**
 * Created by vaibhav on 27-11-2016.
 */
import React from 'react';

const Banner = () => {
    return (
        <section id="banner">
            <div className="inner">

                <header>
                    <h2>The Storygraphers</h2>
                </header>
                <p>We are <strong>The Storygraphers</strong>, an awesome
                    <br/>
                     group of photographers, writers and filmmakers.
                    <br/>
                </p>
                <footer>
                    <ul className="buttons vertical">
                        <li><a href="#main" className="button fit scrolly">Tell Me More</a></li>
                    </ul>
                </footer>
            </div>
        </section>
    );
};

export default Banner;