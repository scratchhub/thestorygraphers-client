/**
 * Created by vaibhav on 29-11-2016.
 */
import React from 'react';

const CTA = () => {
    return (
        <section id="cta">
            <header>
                <h2>Ready to do <strong>something</strong>?</h2>
            </header>
            <footer>
                <ul className="buttons">
                    <li><a href="/contact" className="button special">Talk To Us!</a></li>
                </ul>
            </footer>
        </section>
    );
};

export default CTA;