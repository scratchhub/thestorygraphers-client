import React, {Component} from 'react';

import CTA from  './CTA';
import Footer from '../Footer';

class App extends Component {
    render() {
        return (
            <div id="page-wrapper">
                {this.props.children}
                <CTA/>
                <Footer/>
            </div>
        );
    }
}

export default App;
