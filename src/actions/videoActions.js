/**
 * Created by vaibhav on 27-11-2016.
 */
import * as types from './actionTypes';
import axios from 'axios';

export function fetchVideosSuccess(videos) {
    return {
        type: types.FETCH_VIDEO_SUCCESS,
        payload: videos
    };
}

export function fetchVideos() {
    return function (dispatch) {
        return axios.get('http://thestorygraphers.pythonanywhere.com/api/videos/?format=json').then(function (videos) {
            dispatch(fetchVideosSuccess(videos.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}

export function fetchRecentVideos() {
    return function (dispatch) {
        return axios.get('http://thestorygraphers.pythonanywhere.com/api/recentvideos/?format=json').then(function (videos) {
            dispatch(fetchVideosSuccess(videos.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}