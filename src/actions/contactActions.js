/**
 * Created by vaibhav on 29-11-2016.
 */
import * as types from './actionTypes';
import axios from 'axios';


function sendContactSuccess(contact) {
    return {
        type: types.SEND_CONTACT_SUCCESS,
        payload: contact
    };
}
export function saveContact(contact) {
    return function (dispatch) {
        return axios({
            method: 'post',
            url: 'http://thestorygraphers.pythonanywhere.com/api/contact/',
            data: contact
        }).then(function (about) {
            dispatch(sendContactSuccess(about.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}