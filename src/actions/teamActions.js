/**
 * Created by vaibhav on 27-11-2016.
 */
import * as types from './actionTypes';
import axios from 'axios';

export function fetchTeamSuccess(team) {
    return {
        type: types.FETCH_TEAM_SUCCESS,
        payload: team
    };
}

export function fetchTeam() {
    return function (dispatch) {
        return axios.get('http://thestorygraphers.pythonanywhere.com/api/team/?format=json').then(function (team) {
            dispatch(fetchTeamSuccess(team.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}