/**
 * Created by vaibhav on 27-11-2016.
 */
import * as types from './actionTypes';
import axios from 'axios';

export function fetchArticleSuccess(articles) {
    return {
        type: types.FETCH_ARTICLES_SUCCESS,
        payload: articles
    };
}

export function getArticleSuccess(article) {
    return {
        type: types.GET_ARTICLE_SUCCESS,
        payload: article
    }
}

export function fetchArticles() {
    return function (dispatch) {
        return axios.get('http://thestorygraphers.pythonanywhere.com/api/articles/?format=json').then(function (articles) {
            dispatch(fetchArticleSuccess(articles.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}

export function getArticle(slug) {
    return function (dispatch) {
        return axios.get('http://thestorygraphers.pythonanywhere.com/api/article/' + slug + '/?format=json').then(function (article) {
            dispatch(getArticleSuccess(article.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}