/**
 * Created by vaibhav on 27-11-2016.
 */
import * as types from './actionTypes';
import axios from 'axios';

export function fetchPhotosSuccess(photos) {
    return {
        type: types.FETCH_PHOTOS_SUCCESS,
        payload: photos
    };
}

export function fetchPhotos() {
    return function (dispatch) {
        return axios.get('http://thestorygraphers.pythonanywhere.com/api/photos/?format=json').then(function (photos) {
            dispatch(fetchPhotosSuccess(photos.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}

export function fetchRecentPhotos() {
    return function (dispatch) {
        return axios.get('http://thestorygraphers.pythonanywhere.com/api/recentphotos/?format=json').then(function (photos) {
            dispatch(fetchPhotosSuccess(photos.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}