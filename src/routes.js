/**
 * Created by vaibhav on 27-11-2016.
 */
import React from 'react';
import {IndexRoute, Route} from 'react-router';

import App from './components/App';
import HomePage from './components/HomePage';
import PhotosPage from './components/PhotosPage';
import VideosPage from  './components/VideosPage';
import BlogPage from './components/BlogPage';
import ArticleList from './containers/ArticleList';
import ArticlePage from './containers/ArticlePage';
import ContactPage from './containers/ContactPage';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={HomePage}/>
        <Route path='photography' component={PhotosPage}/>
        <Route path='films' component={VideosPage}/>
        <Route path='contact' component={ContactPage}/>
        <Route path="blog" component={BlogPage}>
            <IndexRoute component={ArticleList}/>
            <Route path="article/:slug" component={ArticlePage}/>
        </Route>
    </Route>
);