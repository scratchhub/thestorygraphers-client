/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import PhotoRow from './PhotoRow';
import * as photoActions from '../../actions/photoActions';

class PhotoList extends Component {
    componentWillMount() {
        this.props.actions.fetchPhotos();
    }

    render() {
        const {photos} = this.props;
        return (
            <section className="wrapper style3 container special">

                <header className="major">
                    <h2>Some of our Photography Projects</h2>
                </header>

                <div className="row">
                    {photos.map(photo =>
                        <PhotoRow key={photo.id} photo={photo}/>
                    )}
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        photos: state.photos
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(photoActions, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(PhotoList);
