/**
 * Created by vaibhav on 29-11-2016.
 */
import React, {Component, PropTypes} from 'react';

class PhotoRow extends Component {
    render() {
        const {photo} = this.props;
        return (
            <div className="4u 12u(narrower)">
                <section>
                    <a href={photo.image} className="image featured"><img src={photo.image} alt=""
                                                                          title={photo.caption}/></a>
                    <header>
                        <h3>{photo.caption}</h3>
                    </header>
                </section>
            </div>
        );
    }
}

PhotoRow.propTypes = {
    photo: PropTypes.object.isRequired
};

export default PhotoRow;