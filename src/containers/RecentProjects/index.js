/**
 * Created by vaibhav on 28-11-2016.
 */
import React, {Component} from 'react';

import PhotoList from './PhotoList';
import FilmList from './FilmList';

class RecentProjects extends Component {
    render() {
        return (
            <section className="wrapper style3 container special">
                <header className="major">
                    <h2>Check out some of our <strong>recent works</strong></h2>
                </header>
                <div className="row" id="mygallery">
                    <div className="6u 12u(narrower)">
                        <header>
                            <h3>Photography</h3>
                        </header>
                        <PhotoList/>
                        <footer className="major">
                            <ul className="buttons">
                                <li><a href="/photography" className="button" data-poptrox="ignore">See More</a></li>
                            </ul>
                        </footer>
                    </div>
                    <div className="6u 12u(narrower)">
                        <header>
                            <h3>Films</h3>
                        </header>
                        <FilmList/>
                        <footer className="major">
                            <ul className="buttons">
                                <li><a href="/films" className="button" data-poptrox="ignore">See More</a></li>
                            </ul>
                        </footer>
                    </div>
                </div>
            </section>
        );
    }
}

export default RecentProjects;