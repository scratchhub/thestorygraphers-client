/**
 * Created by vaibhav on 28-11-2016.
 */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import FilmRow from './FilmRow';
import * as videoActions from '../../actions/videoActions';

class FilmList extends Component {
    componentWillMount() {
        this.props.actions.fetchRecentVideos();
    }

    render() {
        const {videos} = this.props;
        return (
            <div className="row">
                {videos.map(video =>
                    <FilmRow key={video.id} video={video}/>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        videos: state.videos
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(videoActions, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(FilmList);