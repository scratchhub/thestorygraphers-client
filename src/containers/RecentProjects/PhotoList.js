/**
 * Created by vaibhav on 28-11-2016.
 */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import PhotoRow from './PhotoRow';
import * as photoActions from '../../actions/photoActions';

class PhotoList extends Component {
    componentWillMount() {
        this.props.actions.fetchRecentPhotos();
    }

    render() {
        const {photos} = this.props;
        return (
            <div className="row">
                {photos.map(photo =>
                    <PhotoRow key={photo.id} photo={photo}/>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        photos: state.photos
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(photoActions, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(PhotoList);