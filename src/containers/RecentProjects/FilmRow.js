/**
 * Created by vaibhav on 28-11-2016.
 */
import React, {Component} from 'react';

class FilmRow extends Component {
    render() {
        const {video} = this.props;
        return (
            <div className="6u 12u(narrower)">
                <section>
                    <a href={"http://youtu.be/" + video.youtube_code} data-poptrox="youtube,800x480"
                       className="image featured">
                        <img
                            src={video.thumbnail}
                            height="150"
                            alt={video.caption}
                            title={video.caption}/>
                    </a>
                </section>
            </div>
        );
    }
}

export default FilmRow;