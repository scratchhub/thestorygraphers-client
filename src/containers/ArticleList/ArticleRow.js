/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component} from "react";

class ArticleRow extends Component {
    render() {
        const {article} = this.props;
        return (
            <section>
                <br/>
                <a href={"/blog/article/" + article.slug} className="image featured"><img src={article.image}
                                                                                          alt={article.title}/></a>
                <header>
                    <h3><strong>{article.title}</strong></h3>
                    <p>Published on : {article.published_date}</p>
                </header>
                <p>{article.content.substring(0, 200)}</p>
                <a href={"/blog/article/" + article.slug} className="button special">Continue Reading</a>
            </section>
        );
    }
}

export default ArticleRow;