/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import ArticleRow from './ArticleRow';
import * as articleActions from '../../actions/articleActions';

class ArticleList extends Component {
    componentWillMount() {
        this.props.actions.fetchArticles();
    }

    render() {
        const {articles} = this.props;
        return (
            <div>
                <header className="special container">
                    <span className="icon fa-video-camera"/>
                    <h2>The Storygraphers <strong>Blog</strong></h2>
                </header>
                <section className="wrapper style4 container">
                    <div className="row 150%">
                        <div className="row 150%">
                            <div id="content" className="8u 12u(mobile)">
                                {articles.map(article =>
                                    <ArticleRow key={article.id} article={article}/>
                                )}
                            </div>
                            <div className="4u 12u(narrower)">
                                <div className="sidebar">
                                    <a className="twitter-timeline" data-height="1000" data-link-color="#83d3c9"
                                       href="https://twitter.com/thestorygrapher">Tweets by thestorygrapher</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        articles: state.articles
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(articleActions, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(ArticleList);
