/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component} from 'react';

class Member extends Component {
    render() {
        const {member} = this.props;
        return (
            <div className="4u 12u(narrower)">
                <section>
                    <a href="#" className="image featured"><img src={member.thumbnail} alt={member.name}/></a>
                    <header>
                        <h3>{member.name}</h3>
                    </header>
                    <p style={{fontSize: "small"}}>{member.description}</p>
                </section>
            </div>
        );
    }
}

export default Member;