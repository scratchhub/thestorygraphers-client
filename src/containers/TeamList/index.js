/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Member from './Member';
import * as teamActions from '../../actions/teamActions';

class TeamList extends Component {
    componentWillMount() {
        this.props.actions.fetchTeam();
    }

    render() {
        const {team} = this.props;
        return (
            <section className="wrapper style1 container special" id="team">
                <h2>Meet The Team</h2>
                <div className="row">
                    {team.slice(0, 3).map(member =>
                        <Member key={member.id} member={member}/>
                    )}
                </div>
                <div className="row">
                    {team.slice(3).map(member =>
                        <Member key={member.id} member={member}/>
                    )}
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        team: state.team
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(teamActions, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(TeamList);