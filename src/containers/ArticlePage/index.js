import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as articleActions from '../../actions/articleActions';

class ArticlePage extends Component {
    componentWillMount() {
        this.props.actions.getArticle(this.props.params.slug);
    }

    render() {
        const {article} = this.props;
        return (
            <div>
                <header className="special container">
                    <span className="icon fa-video-camera"/>
                    <h2>{article.title}</h2>
                    <p>{article.published_date}</p>
                </header>
                <section className="wrapper style4 container">
                    <div className="content">
                        <section>
                            <a href="#" className="image featured">
                                <img src={"http://thestorygraphers.pythonanywhere.com" + article.image}
                                     alt={article.title}/>
                            </a>
                            <header>
                                <h3>{article.subtitle}</h3>
                            </header>
                            <p>{article.content}</p>
                        </section>
                        <div className="fb-comments"
                             data-href={"http://www.storygraphers.in/blog/article/" + article.slug}
                             data-numposts="5"
                             data-width="100%"></div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        article: state.articles
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(articleActions, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(ArticlePage);
