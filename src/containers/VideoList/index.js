/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import VideoRow from './VideoRow';
import * as videoActions from '../../actions/videoActions';

class VideoList extends Component {
    componentWillMount() {
        this.props.actions.fetchVideos();
    }

    render() {
        const {videos} = this.props;
        return (
            <section className="wrapper style3 container special">

                <header className="major">
                    <h2>Some of our Filmmaking Projects</h2>
                </header>

                <div className="row">
                    {videos.map(video =>
                        <VideoRow key={video.id} video={video}/>
                    )}
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        videos: state.videos
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(videoActions, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(VideoList);
