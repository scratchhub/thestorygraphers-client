/**
 * Created by vaibhav on 29-11-2016.
 */
import React, {Component, PropTypes} from 'react';

class VideoRow extends Component {
    render() {
        const {video} = this.props;
        return (
            <div className="4u 12u(narrower)">
                <section>
                    <a href={"http://youtu.be/" + video.youtube_code} data-poptrox="youtube,800x480"
                       className="image featured">
                        <img
                            src={video.thumbnail}
                            height="150"
                            alt={video.caption}
                            title={video.caption}/>
                    </a>
                    <header>
                        <h3>{video.caption}</h3>
                    </header>
                </section>
            </div>
        );
    }
}

VideoRow.propTypes = {
    video: PropTypes.object.isRequired
};

export default VideoRow;