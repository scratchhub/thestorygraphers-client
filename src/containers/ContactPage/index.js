/**
 * Created by vaibhav on 27-11-2016.
 */
import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Header from '../../components/Header';
import ContactForm from '../../components/ContactForm';
import * as contactActions from '../../actions/contactActions';

class ContactPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            contact: Object.assign({}, props.contact),
            errors: {}
        };

        this.updateContactState = this.updateContactState.bind(this);
        this.sendContact = this.sendContact.bind(this);
    }

    updateContactState(event) {
        const field = event.target.name;
        let contact = this.state.contact;
        contact[field] = event.target.value;
        return this.setState({contact: contact});
    }

    sendContact(event){
        event.preventDefault();
        this.props.actions.saveContact(this.state.contact);
        alert("Your Message Was Successfully Sent");
        this.context.router.push('/');
    }

    render() {
        return (
            <div>
                <Header/>
                <article id="main">

                    <header className="special container">
                        <span className="icon fa-envelope"/>
                        <h2>Got Something In Mind? Get In Touch!</h2>
                    </header>
                    <section className="wrapper style4 special container 75%">
                        <div className="content">
                        </div>
                        <ContactForm onSave={this.sendContact} onChange={this.updateContactState} errors={this.state.errors}/>
                    </section>
                </article>
            </div>
        );
    }
}

ContactPage.propTypes = {
    contact: PropTypes.object,
    actions: PropTypes.object.isRequired
};

ContactPage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        contact: state.contact
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(contactActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactPage);