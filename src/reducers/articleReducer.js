/**
 * Created by vaibhav on 27-11-2016.
 */
import * as types from '../actions/actionTypes';

export default function articleReducer(state = [], action) {
    switch (action.type){
        case types.FETCH_ARTICLES_SUCCESS:
            return action.payload;
        case types.GET_ARTICLE_SUCCESS:
            return action.payload
        default:
            return state;
    }
}