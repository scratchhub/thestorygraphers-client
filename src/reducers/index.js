/**
 * Created by vaibhav on 27-11-2016.
 */
import {combineReducers} from 'redux';

import teamReducer from './teamReducer';
import articleReducer from './articleReducer';
import photoReducer from './photoReducer';
import videoReducer from './videoReducer';
import contactReducer from './contactReducer';

const rootReducer = combineReducers({
    team: teamReducer,
    articles: articleReducer,
    photos: photoReducer,
    videos: videoReducer,
    contact: contactReducer,
});

export default rootReducer;