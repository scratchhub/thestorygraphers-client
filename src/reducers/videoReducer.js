/**
 * Created by vaibhav on 27-11-2016.
 */
import * as types from '../actions/actionTypes';

export default function videoReducer(state = [], action) {
    switch (action.type) {
        case types.FETCH_VIDEO_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}