/**
 * Created by vaibhav on 27-11-2016.
 */
import * as types from '../actions/actionTypes';

export default function teamReducer(state = [], action) {
    switch (action.type){
        case types.FETCH_TEAM_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}