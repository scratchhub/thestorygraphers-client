/**
 * Created by vaibhav on 29-11-2016.
 */
import * as types from '../actions/actionTypes';

export default function contactReducer(state = [], action) {
    switch (action.type) {
        case types.SEND_CONTACT_SUCCESS:
            return [
                ...state,
                Object.assign({}, action.payload)
            ];
        default:
            return state;
    }
}